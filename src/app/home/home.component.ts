import { Component, OnInit } from '@angular/core';
import {moveIn} from '../router.animations';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

interface DetailNode {
  name: string;
  children? : DetailNode[];
}

const TREE_DATA: DetailNode[] = [
  {name: 'my orders', children: [ {name: 'laptop'}, {name: 'mobile'}, {name: 'kurti sets'}, {name: 'skin care products'},
]
},
{name: 'my offers', children: [ {name: 'offers on electronic gadgets'}, {name: 'offers on skincare products'},
{name: 'offers on fashion clothes'},
]
},
];

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [moveIn()],
  host: {'[@moveIn]': ''}
})
export class HomeComponent implements OnInit {
  //code for tree data
  private _transformer = (node:DetailNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length >0,
      name: node.name,
      level: level,
    };

  }
  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node =>node.expandable);
    treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node =>node.children
    );
    dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  state: string = '';
  opened = false;

  constructor() { this.dataSource.data = TREE_DATA; }
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit(): void {
  }

}
