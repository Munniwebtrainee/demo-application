import { Component, OnInit } from '@angular/core';
import {moveIn} from '../router.animations';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  animations: [moveIn()],
  host: {'[@moveIn]': ''}
})
export class ContactComponent implements OnInit {
  state: string = '';
  hide = true;  
  disableSelect = new FormControl(false);
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if(this.email.hasError('required')) {
      return 'you must enter a valid email';
    }
    return this.email.hasError('email') ? 'not a valid email': '';
  }

  constructor() { }

  ngOnInit(): void {
  }

}
