import { Component, OnInit, ViewChild } from '@angular/core';
import {moveIn} from '../router.animations';
import {MatSort} from '@angular/material/sort';
import { DataService } from '../data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  animations: [moveIn()],
  host: {'[@moveIn]': ''}
})
export class AboutComponent implements OnInit {
  state: string = '';
@ViewChild(MatSort, {static: true}) sort: MatSort;

  //code for http getting data
  
  constructor(private dataService: DataService) {}
displayedColumns: string[] = ['category', 'subcategory', 'name', 'price'];

public Product = [];
  ngOnInit() {
//this.Product.sort = this.sort;

    this.dataService.getProducts().subscribe(data => this.Product = data);
  }
 
  
}

