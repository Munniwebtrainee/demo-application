import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


export class ProductsData {
  category: string;
  name: string;
  price: number;
  subcategory: string;
}



@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _url: string = "/assets/productlist.json";


  constructor(private _http:HttpClient) { }

  getProducts():Observable<ProductsData[]> {
    return this._http.get<ProductsData[]>(this._url);
  }
}
